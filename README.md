# GEOLOCPVT

GeolocPVT is a project aiming to visualize GNSS raw data on Android phones. It has been launch to
contribute to the [GNSS Raw Measurements Task Force](https://www.gsa.europa.eu/gnss-applications/gnss-raw-measurements/gnss-raw-measurements-task-force) led by the GSA.

GeolocPVT is distributed as a free software in order to build a community of users, contributors,
developers who will contribute to the project and ensure the necessary means for its evolution.

GeolocPVT is a free software; you can redistribute it and/or modify it under the terms of the
GNU Lesser General Public License as published by the Free Software Foundation; either version 3
of the License, or (at your option) any later version. Any modification of source code in this
LGPL software must also be published under the LGPL license.

GeolocPVT is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

You should have received a copy of the GNU Lesser General Public License along with GeolocPVT.
If not, see <https://www.gnu.org/licenses/lgpl.txt/>.
 


## Getting Started

### Prerequisites

All code is running in Android Studio.

Android minimal version : API 26

It is recommended to use the Xiaomi Mi 8 phone to access all the options of the app.

### Starting

For more informations, please see the developer_guide in the documentation folder or in the [Gitlab](https://gitlab.com/TeamGEOLOC/geolocpvt/-/tree/master/documentation)

## Versioning

We use [Gitlab](https://about.gitlab.com/) for versioning. For the versions available, see the [repository](https://gitlab.com/TeamGEOLOC/geolocpvt). 

## Contributing

If you want to contribute to this project, please follow this rules :

1 - Fork the project on your own Gitlab space

2 - Clone it on your computer
3 - Create your feature branch (git checkout -b my-new-feature)

4 - Commit your changes (git commit -am "Add some features")

5 - Push to the branch (git push origin my-new-feature)

6 - Create a new Pull Request


Please respect the camelCase and the JavaDoc.
Don't forget to comment your code.

## Authors

See the list of contributors who participated in this project in the AUTHORS.md file.

## License

See the LICENCE.md file.

