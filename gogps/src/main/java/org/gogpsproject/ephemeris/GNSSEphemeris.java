///=================================================================================================
// Class GNSSEphemeris
//      Author :  Antoine GRENIER
//        Date :  2019/09/06
///=================================================================================================
/*
 * Copyright 2018(c) IFSTTAR - TeamGEOLOC
 *
 * This file is part of the GeolocPVT application.
 *
 * GeolocPVT is distributed as a free software in order to build a community of users, contributors,
 * developers who will contribute to the project and ensure the necessary means for its evolution.
 *
 * GeolocPVT is a free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version. Any modification of source code in this
 * LGPL software must also be published under the LGPL license.
 *
 * GeolocPVT is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the GNU Lesser General Public License along with GeolocPVT.
 * If not, see <https://www.gnu.org/licenses/lgpl.txt/>.
 */
///=================================================================================================

package org.gogpsproject.ephemeris;

public class GNSSEphemeris {

    private int prn;
    private int gnssSystem;

    private int iode;       //Issue of Data Ephemeris
    private int iodc;       //Issue of Data Clock
    private int svh;        //Satellite health
    private int svdv;       //Data validity status (See GALILEO ICD)
    private int sisa;       //SIS Accuracy

    //subframe 1 (time corrections)
    private int wn;         //week number
    private double toc;     //clock data reference time in seconds
    private double af2;     //polynomial coefficient (sec/sec^2)
    private double af1;     //polynomial coefficient (sec/sec)
    private double af0;     //polynomial coefficient (seconds)

    //subframe 2 (ephemerides part 1)
    private double crs;     //Amplitude of the Sine Harmonic Correction Term to the Orbit Radius
    private double deltaN;  //Mean Motion Difference From Computed Value
    private double m0;      //Mean Anomaly at Reference Time
    private double cuc;     //Amplitude of the Cosine Harmonic Correction Term to the Argument of Latitude
    private double ec;      //Eccentricity
    private double cus;     //Amplitude of the Sine Harmonic Correction Term to the Argument of Latitude
    private double squareA; //Square Root of the Semi-Major Axis
    private double toe;     //Reference Time Ephemeris
    private double aodo;    //Age of data offset

    //subframe 3 (ephemerides part 2)
    private double cic;     //Amplitude of the Cosine Harmonic Correction Term to the Angle of Inclination
    private double omega0;  //Longitude of Ascending Node of Orbit Plane at Weekly Epoch
    private double cis;     //Amplitude of the Sine Harmonic Correction Term to the Angle of Inclination
    private double i0;      //Inclination Angle at Reference Time
    private double crc;     //Amplitude of the Cosine Harmonic Correction Term to the Orbit Radius
    private double omega;   //Argument of Perigee
    private double omegaDot;//Rate of Right Ascension
    private double idot;    //Rate of Inclination Angle

    // Corrections
    private PreciseCorrection mEphCorrections;
    private SatelliteCodeBiases.CodeBias codeBias;

    //----------------------------------------------------------------------------------------------

    /**
     * Constructor.
     */
    public GNSSEphemeris()
    {
    }

    //----------------------------------------------------------------------------------------------

    /**
     * Constructor of recopy.
     * @param eph Ephemeris
     */
    public GNSSEphemeris(GNSSEphemeris eph)
    {
        setPrn(eph.getPrn());
        setGnssSystem(eph.getGnssSystem());
        setWeek(eph.getWn());
        setIdot(eph.getIdot());
        setIode(eph.getIode());
        setToc(eph.getToc());
        setToe(eph.getToe());
        setAf2(eph.getAf2());
        setAf1(eph.getAf1());
        setAf0(eph.getAf0());
        setIodc(eph.getIodc());
        setCrs(eph.getCrs());
        setCrc(eph.getCrc());
        setDeltaN(eph.getDeltaN());
        setM0(eph.getM0());
        setCuc(eph.getCuc());
        setEc(eph.getEc());
        setCus(eph.getCus());
        setSquareA(eph.getSquareA());
        setToe(eph.getToe());
        setCic(eph.getCic());
        setOmega0(eph.getOmega0());
        setCis(eph.getCis());
        setI0(eph.getI0());
        setOmega(eph.getOmega());
        setOmegaDot(eph.getOmegaDot());
        setSvh(eph.getSvh());
        setSvdv(eph.getSvdv());
        setSisa(eph.getSisa());

        if(eph.getCodeBias() != null)
        {
            setCodeBias(new SatelliteCodeBiases.CodeBias(eph.getCodeBias()));
        }
        if(eph.getEphCorrections() != null)
        {
            setEphCorrections(eph.getEphCorrections());
        }
    }

    //----------------------------------------------------------------------------------------------

    public PreciseCorrection getEphCorrections() {
        return mEphCorrections;
    }

    //----------------------------------------------------------------------------------------------

    public void setEphCorrections(PreciseCorrection mEphemerisGNSSCorr) {
        this.mEphCorrections = mEphemerisGNSSCorr;
    }

    //----------------------------------------------------------------------------------------------

    public Object copy()
    {
        return new GNSSEphemeris(this);
    }

    //----------------------------------------------------------------------------------------------

    public void setPrn(int _prn)
    {
        this.prn = _prn;
    }

    public void setGnssSystem(int _sys)
    {
        this.gnssSystem = _sys;
    }

    public void setWeek(int _wn)
    {
        wn = _wn;
    }

    public void setIdot(double _idot)
    {
        idot = _idot;
    }

    public void setIode(int _iode)
    {
        iode = _iode;
    }

    public void setToc(double _toc)
    {
        toc = _toc;
    }

    public void setAf2(double af2) {
        this.af2 = af2;
    }

    public void setAf1(double af1) {
        this.af1 = af1;
    }

    public void setAf0(double af0) {
        this.af0 = af0;
    }

    public void setIodc(int iodc) {
        this.iodc = iodc;
    }

    public void setCrs(double crs) {
        this.crs = crs;
    }

    public void setCrc(double crc) {
        this.crc = crc;
    }

    public void setDeltaN(double deltaN) {
        this.deltaN = deltaN;
    }

    public void setM0(double m0) {
        this.m0 = m0;
    }

    public void setCuc(double cuc) {
        this.cuc = cuc;
    }

    public void setEc(double ec) {
        this.ec = ec;
    }

    public void setCus(double cus) {
        this.cus = cus;
    }

    public void setSquareA(double squareA) {
        this.squareA = squareA;
    }

    public void setToe(double toe) {
        this.toe = toe;
    }

    public void setCic(double cic) {
        this.cic = cic;
    }

    public void setOmega0(double omega0) {
        this.omega0 = omega0;
    }

    public void setCis(double cis) {
        this.cis = cis;
    }

    public void setI0(double i0) {
        this.i0 = i0;
    }

    public void setOmega(double omega) {
        this.omega = omega;
    }

    public void setOmegaDot(double omegaDot) {
        this.omegaDot = omegaDot;
    }

    public int getSisa() {
        return sisa;
    }

    public void setSisa(int sisa) {
        this.sisa = sisa;
    }

    public void setSvh(int svh) {
        this.svh = svh;
    }

    public int getPrn() {
        return prn;
    }

    public int getGnssSystem() {
        return gnssSystem;
    }

    public int getWn() {
        return wn;
    }

    public int getIode() {
        return iode;
    }

    public int getIodc() {
        return iodc;
    }

    public int getSvh() {
        return svh;
    }

    public double getToc() {
        return toc;
    }

    public double getAf2() {
        return af2;
    }

    public double getAf1() {
        return af1;
    }

    public double getAf0() {
        return af0;
    }

    public double getCrs() {
        return crs;
    }

    public double getDeltaN() {
        return deltaN;
    }

    public double getM0() {
        return m0;
    }

    public double getCuc() {
        return cuc;
    }

    public double getEc() {
        return ec;
    }

    public double getCus() {
        return cus;
    }

    public double getSquareA() {
        return squareA;
    }

    public double getToe() {
        return toe;
    }

    public double getAodo() {
        return aodo;
    }

    public double getCic() {
        return cic;
    }

    public double getOmega0() {
        return omega0;
    }

    public double getCis() {
        return cis;
    }

    public double getI0() {
        return i0;
    }

    public double getCrc() {
        return crc;
    }

    public double getOmega() {
        return omega;
    }

    public double getOmegaDot() {
        return omegaDot;
    }

    public double getIdot() {
        return idot;
    }

    public int getSvdv() {
        return svdv;
    }

    public void setSvdv(int svdv) {
        this.svdv = svdv;
    }

    public SatelliteCodeBiases.CodeBias getCodeBias() {
        return codeBias;
    }

    public void setCodeBias(SatelliteCodeBiases.CodeBias codeBias) {
        this.codeBias = codeBias;
    }

    @Override
    public String toString() {
        String str = gnssSystem +
                "," + prn +
                "," + wn +
                "," + toc +
                "," + iode +
                "," + af2 +
                "," + af1 +
                "," + af0 +
                "," + crs +
                "," + deltaN +
                "," + m0 +
                "," + cuc +
                "," + ec +
                "," + cus +
                "," + squareA +
                "," + toe +
                "," + aodo +
                "," + cic +
                "," + omega0 +
                "," + cis +
                "," + i0 +
                "," + crc +
                "," + omega +
                "," + omegaDot +
                "," + idot;
        return str;
    }

}
